const mongoose = require('mongoose');
const slugify = require('slugify');
// const User = require('./userModel');
// const validator = require('validator');

const restaurantSchema = new mongoose.Schema(
    {
        Name: {
            type: String,
            //required: [true, 'A restaurant must have a name'],
            unique: true,
            trim: true
            //            maxlength: [40, 'A restaurant name must have less or equal then 40 characters'],
            //            minlength: [10, 'A restaurant name must have more or equal then 10 characters']
            // validate: [validator.isAlpha, 'restaurant name must only contain characters']
        },
        slug: String,
        ShortDescription: {
            type: String,
            trim: true
        },
        FullDescription: {
            type: String,
            trim: true
        },
        Product_Url: String,
        Pic_Url: String,
        Accessibility: Boolean,
        Address: { 
            type: String,
            required: [true, 'A restaurant must have an Address']
        },
        City: String,
        Email: String,
        Fax: String,
        Opening_Hours: String,
        Parking: Boolean,
        Phone: String,
        URL: String,
        X: Number,//longitude
        Y: Number,//latitude
        Location: {
            // GeoJSON
            type: {
                type: String,
                default: 'Point',
                enum: ['Point']
            },
            coordinates: [Number]
        },
        Type_of_Kashrut: {
            type: String,
            enum: {
                values: ['ללא כשרות', 'כשר', 'כשר למהדרין', 'בהשגחת הבד"ץ'],
                message: 'either there isnt , there is , casher lemehadrin , or behashgahat habadats'
            }
        },
        ratingsAverage: {
            type: Number,
            default: 4.5,
            min: [1, 'Rating must be above 1.0'],
            max: [5, 'Rating must be below 5.0'],
            set: val => Math.round(val * 10) / 10 // 4.666666, 46.6666, 47, 4.7
        },
        ratingsQuantity: {
            type: Number,
            default: 0
        },
        imageCover: {
            type: String,
        //    required: [true, 'A restaurant must have a cover image']
        },
        images: [String],
        createdAt: {
            type: Date,
            default: Date.now(),
            select: false
        }
    },
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
);

// restaurantSchema.index({ price: 1 });
//restaurantSchema.index({ price: 1, ratingsAverage: -1 });
restaurantSchema.index({ slug: 1 });
restaurantSchema.index({ Location: '2dsphere' });

restaurantSchema.virtual('durationWeeks').get(function() {
    return this.duration / 7;
});

// Virtual populate
//restaurantSchema.virtual('reviews', {
//    ref: 'Review',
//    foreignField: 'restaurant',
//    localField: '_id'
//});

// DOCUMENT MIDDLEWARE: runs before .save() and .create()
restaurantSchema.pre('save', function(next) {
    //this.slug = slugify(this.Name, { lower: true });// this doesnt work with hebrew
    this.slug = this.Name.replace(/\s+/g,'-');
    next();
});

// restaurantSchema.pre('save', async function(next) {
//   const guidesPromises = this.guides.map(async id => await User.findById(id));
//   this.guides = await Promise.all(guidesPromises);
//   next();
// });

// restaurantSchema.pre('save', function(next) {
//   console.log('Will save document...');
//   next();
// });

// restaurantSchema.post('save', function(doc, next) {
//   console.log(doc);
//   next();
// });

// QUERY MIDDLEWARE
// restaurantSchema.pre('find', function(next) {
//restaurantSchema.pre(/^find/, function(next) {
//    this.find({ secretrestaurant: { $ne: true } });
//
//    this.start = Date.now();
//    next();
//});
//
//restaurantSchema.pre(/^find/, function(next) {
//    this.populate({
//        path: 'guides',
//        select: '-__v -passwordChangedAt'
//    });
//
//    next();
//});

restaurantSchema.post(/^find/, function(docs, next) {
    console.log(`Query took ${Date.now() - this.start} milliseconds!`);
    next();
});

// AGGREGATION MIDDLEWARE
// restaurantSchema.pre('aggregate', function(next) {
//   this.pipeline().unshift({ $match: { secretrestaurant: { $ne: true } } });

//   console.log(this.pipeline());
//   next();
// });

const Restaurant = mongoose.model('Restaurant', restaurantSchema);

module.exports = Restaurant;
