const Rest = require('../models/restaurantModel');
const User = require('../models/userModel');
const catchAsync = require('../utils/catchAsync');
const AppError = require('../utils/appError');
const APIFeatures = require('./../utils/apiFeatures');

exports.getOverview = catchAsync(async (req, res, next) => {
    // 1) Get tour data from collection
    //const rests = await Rest.find();
    const features = new APIFeatures(Rest.find(), req.query)
      .filter()
      .sort()
      .limitFields()
      .paginate();
    
    const rests = await features.query;

    // 2) Build template
    // 3) Render that template using tour data from 1)
    res.status(200).render('overview', {
        title: 'All Restaurants',
        rests
    });
});

exports.getRest = catchAsync(async (req, res, next) => {
    // 1) Get the data, for the requested tour (including reviews and guides)
    //  const rest = await Rest.findOne({ slug: req.params.slug }).populate({
    //    path: 'reviews',
    //    fields: 'review rating user'
    //  });
    slug = decodeURIComponent(req.params.slug);
    console.log(slug);
    const rest = await Rest.findOne({ slug });
    if (!rest) {
        return next(new AppError('There is no restaurant with that name.', 404));
    }

    // 2) Build template
    // 3) Render template using data from 1)
    res.status(200).render('rest', {
        title: `${rest.name} restaurant`,
        rest
    });
});

exports.getLoginForm = (req, res) => {
    res.status(200).render('loginSignup', {
    });
};

exports.getSignUpForm = (req, res) => {
    res.status(200).render('loginSignup', {
        signup: true
    });
};

exports.getAccount = (req, res) => {
    res.status(200).render('account', {
        title: 'Your account'
    });
};

exports.updateUserData = catchAsync(async (req, res, next) => {
    const updatedUser = await User.findByIdAndUpdate(
        req.user.id,
        {
            name: req.body.name,
            email: req.body.email
        },
        {
            new: true,
            runValidators: true
        }
    );

    res.status(200).render('account', {
        title: 'Your account',
        user: updatedUser
    });
});
