const restaurant = require('./../models/restaurantModel');
const catchAsync = require('./../utils/catchAsync');
const factory = require('./handlerFactory');
const AppError = require('./../utils/appError');

exports.aliasTopRestaurants = (req, res, next) => {
  req.query.limit = '5';
  req.query.sort = '-ratingsAverage,price';
  req.query.fields = 'name,price,ratingsAverage,summary,difficulty';
  next();
};

exports.getAllRestaurants = factory.getAll(restaurant);
exports.getRestaurant = factory.getOne(restaurant, { path: 'reviews' });
exports.createRestaurant = factory.createOne(restaurant);
exports.updateRestaurant = factory.updateOne(restaurant);
exports.deleteRestaurant = factory.deleteOne(restaurant);

exports.getRestaurantStats = catchAsync(async (req, res, next) => {
  const stats = await restaurant.aggregate([
    {
      $match: { ratingsAverage: { $gte: 4.5 } }
    },
    {
      $group: {
        _id: { $toUpper: '$difficulty' },
        numRestaurants: { $sum: 1 },
        numRatings: { $sum: '$ratingsQuantity' },
        avgRating: { $avg: '$ratingsAverage' },
          //        avgPrice: { $avg: '$price' },
          //        minPrice: { $min: '$price' },
          //        maxPrice: { $max: '$price' }
      }
    },
    {
      $sort: { avgPrice: 1 }
    }
    // {
    //   $match: { _id: { $ne: 'EASY' } }
    // }
  ]);

  res.status(200).json({
    status: 'success',
    data: {
      stats
    }
  });
});

//exports.getMonthlyPlan = catchAsync(async (req, res, next) => {
//  const year = req.params.year * 1; // 2021
//
//  const plan = await restaurant.aggregate([
//    {
//      $unwind: '$startDates'
//    },
//    {
//      $match: {
//        startDates: {
//          $gte: new Date(`${year}-01-01`),
//          $lte: new Date(`${year}-12-31`)
//        }
//      }
//    },
//    {
//      $group: {
//        _id: { $month: '$startDates' },
//        numRestaurantStarts: { $sum: 1 },
//        restaurants: { $push: '$name' }
//      }
//    },
//    {
//      $addFields: { month: '$_id' }
//    },
//    {
//      $project: {
//        _id: 0
//      }
//    },
//    {
//      $sort: { numrestaurantStarts: -1 }
//    },
//    {
//      $limit: 12
//    }
//  ]);
//
//  res.status(200).json({
//    status: 'success',
//    data: {
//      plan
//    }
//  });
//});

// /restaurants-within/:distance/center/:latlng/unit/:unit
// /restaurants-within/233/center/34.111745,-118.113491/unit/mi
exports.getRestaurantsWithin = catchAsync(async (req, res, next) => {
  const { distance, latlng, unit } = req.params;
  const [lat, lng] = latlng.split(',');

  const radius = unit === 'mi' ? distance / 3963.2 : distance / 6378.1;

  if (!lat || !lng) {
    next(
      new AppError(
        'Please provide latitutr and longitude in the format lat,lng.',
        400
      )
    );
  }

  const restaurants = await restaurant.find({
    Location: { $geoWithin: { $centerSphere: [[lng, lat], radius] } }
  });

  res.status(200).json({
    status: 'success',
    results: restaurants.length,
    data: {
      data: restaurants
    }
  });
});

exports.getDistances = catchAsync(async (req, res, next) => {//distances to all the tours from a certain point
  const { latlng, unit } = req.params;
  const [lat, lng] = latlng.split(',');

  const multiplier = unit === 'mi' ? 0.000621371 : 0.001;

  if (!lat || !lng) {
    next(
      new AppError(
        'Please provide latitutr and longitude in the format lat,lng.',
        400
      )
    );
  }

  const distances = await restaurant.aggregate([
    {
      $geoNear: {
        near: {
          type: 'Point',
          coordinates: [lng * 1, lat * 1]
        },
        distanceField: 'distance',
        distanceMultiplier: multiplier
      }
    },
    {
      $project: {
        _id: 0,
        distance: 1,
        Name: 1
      }
    }
  ]);

  res.status(200).json({
    status: 'success',
    data: {
      data: distances
    }
  });
});
