/* eslint-disable */

export const displayMap = loc => {

    mapboxgl.accessToken =
        'pk.eyJ1IjoibG1hbzM2NDYiLCJhIjoiY2p6aWY2YXExMTRxeDNjcWszeXFkaTM5OSJ9.zw5PAWvGNjcT8ph7fDLjYQ';

    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        scrollZoom: false,
        // center: [-118.113491, 34.111745],
        zoom: 8,
        maxZoom: 15,
        minZoom: 7

        // interactive: false
    });

    const bounds = new mapboxgl.LngLatBounds();

    // Create marker
    const el = document.createElement('div');
    el.className = 'marker';

    // Add marker
    new mapboxgl.Marker({
        element: el,
        anchor: 'bottom'
    }).setLngLat(loc.coordinates).addTo(map);

    // Add popup

    // Extend map bounds to include current location
    bounds.extend(loc.coordinates);

    map.fitBounds(bounds,{
      padding: {
        top: 200,
        bottom: 150,
        left: 100,
        right: 100
      }
    });
};
