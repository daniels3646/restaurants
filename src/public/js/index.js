/* eslint-disable */
import '@babel/polyfill';
import { displayMap } from './mapbox';
import { login, logout, signup} from './loginSignup';
import { updateSettings } from './updateSettings';
import { showAlert } from './alerts';
import axios from 'axios';
// DOM ELEMENTS
const mapBox = document.getElementById('map');
const loginForm = document.querySelector('.form--login');
const signupForm = document.querySelector('.form--signup');
const logOutBtn = document.querySelector('.nav__el--logout');
const userDataForm = document.querySelector('.form-user-data');
const userPasswordForm = document.querySelector('.form-user-password');
const search = document.getElementById('search');

// DELEGATION

$(document).ready(function() {
    if (search) {
        search.addEventListener('click', async (e) => {
            let loc = ''
            try {
                const res = await axios(`https://geocode.xyz/${document.getElementById('place').value}, Israel?json=1&auth=286423471822085225312x3324`, {
                    method: 'GET',
                });
                if (JSON.stringify(res.data.alt) === '{}'){
                    showAlert('error','לא נמצא');
                }else{
                    loc = `?location=${res.data.latt},${res.data.longt},${document.getElementById('distance').value}`;
                    location.assign(`${loc}&Parking=${document.getElementById('parking').checked}&Accessibility=${document.getElementById('access').checked}`);
                }
            } catch (err) {
                console.log(err);
                showAlert('error', err.response.data.message);
            }
        });
    }
});

if (mapBox) {
    const location = JSON.parse(mapBox.dataset.location);
    console.log(location)
    displayMap(location);
}

if (loginForm)
    loginForm.addEventListener('submit', e => {
        e.preventDefault();
        const email = document.getElementById('email').value;
        const password = document.getElementById('password').value;
        login(email, password);
    });

if (signupForm)
    signupForm.addEventListener('submit', e => {
        e.preventDefault();
        const name = document.getElementById('name').value;
        const email = document.getElementById('email').value;
        const password = document.getElementById('password').value;
        const confirmpassword = document.getElementById('confirmpassword').value;
        signup(name, email, password, confirmpassword);
    });

if (logOutBtn) logOutBtn.addEventListener('click', logout);

if (userDataForm)
    userDataForm.addEventListener('submit', e => {
        e.preventDefault();
        const name = document.getElementById('name').value;
        const email = document.getElementById('email').value;
        updateSettings({ name, email }, 'data');
    });

if (userPasswordForm)
    userPasswordForm.addEventListener('submit', async e => {
        e.preventDefault();
        document.querySelector('.btn--save-password').textContent = 'Updating...';

        const passwordCurrent = document.getElementById('password-current').value;
        const password = document.getElementById('password').value;
        const passwordConfirm = document.getElementById('password-confirm').value;
        await updateSettings(
            { passwordCurrent, password, passwordConfirm },
            'password'
        );

        document.querySelector('.btn--save-password').textContent = 'Save password';
        document.getElementById('password-current').value = '';
        document.getElementById('password').value = '';
        document.getElementById('password-confirm').value = '';
    });
