const fs = require('fs');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
//const Tour = require('./../../models/tourModel');
const restaurants = require('./../../models/restaurantModel');
//const Review = require('./../../models/reviewModel');
//const User = require('./../../models/userModel');

dotenv.config({ path: './config.env' });

const DB = process.env.DATABASE.replace(
  '<PASSWORD>',
  process.env.DATABASE_PASSWORD_REMOTE
);
//const DB = process.env.DATABASE_LOCAL;

mongoose
    .connect(DB, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false
    })
    .then(() => console.log('DB connection successful!'));

// READ JSON FILE
const Restaurants = JSON.parse(fs.readFileSync(`${__dirname}/israelRestaurantsFormatted.json`, 'utf-8'));

// IMPORT DATA INTO DB
const importData = async () => {
    try {
        await restaurants.create(Restaurants);
        //        await User.create(users, { validateBeforeSave: false });
        //        await Review.create(reviews);
        console.log('Data successfully loaded!');
    } catch (err) {
        console.log(err);
    }
    process.exit();
};

// DELETE ALL DATA FROM DB
const deleteData = async () => {
    try {
        //        await Tour.deleteMany();
        //        await User.deleteMany();
        //        await Review.deleteMany();
        await restaurants.deleteMany();
        console.log('Data successfully deleted!');
    } catch (err) {
        console.log(err);
    }
    process.exit();
};

// CONVERT DATA FROM X AND Y COORDINATES TO GEOJSON
const convertData = async () => {
    //console.log(Restaurants[0].X);
    withLocation = Restaurants.map(rest => (
        {...rest, Location: {
            type : "Point",
            coordinates: [Number(rest.X) , Number(rest.Y)]
        },
            X: undefined,
            Y: undefined,
            Accessibility: rest.Accessibility === 'כן' ? true : false,
            Parking: rest.Parking === 'לא' ? false : true//should make it a enum at some point
        }
    ));
    try {
        //fs.writeFileSync('./dataGeo.json', withLocation, 'utf-8'); 
        await restaurants.create(withLocation);
        console.log('Data successfully converted!');
    } catch (err) {
        console.log(err);
    }
    process.exit();
};

if (process.argv[2] === '--import') {
    importData();
} else if (process.argv[2] === '--delete') {
    deleteData();
} else if (process.argv[2] === '--convert') {
    convertData();
}
