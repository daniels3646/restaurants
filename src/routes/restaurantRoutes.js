const express = require('express');
const restaurantController = require('./../controllers/restaurantController');
const authController = require('./../controllers/authController');
//const reviewRouter = require('./../routes/reviewRoutes');

const router = express.Router();

// router.param('id', restaurantController.checkID);

// POST /restaurant/234fad4/reviews
// GET /restaurant/234fad4/reviews

//router.use('/:restaurantId/reviews', reviewRouter);

//router
//  .route('/top-5-cheap')
//  .get(restaurantController.aliasToprestaurants, restaurantController.getAllrestaurants);

//router.route('/restaurant-stats').get(restaurantController.getrestaurantStats);
//router
//  .route('/monthly-plan/:year')
//  .get(
//    authController.protect,
//    authController.restrictTo('admin', 'lead-guide', 'guide'),
//    restaurantController.getMonthlyPlan
//  );

router
  .route('/restaurants-within/:distance/center/:latlng/unit/:unit')
  .get(restaurantController.getRestaurantsWithin);
// /restaurants-within?distance=233&center=-40,45&unit=mi
// /restaurants-within/233/center/-40,45/unit/mi

router.route('/distances/:latlng/unit/:unit').get(restaurantController.getDistances);

//{{URL}}api/v1/restaurants?duration[gte]=7&sort=price
router
  .route('/')
  .get(restaurantController.getAllRestaurants)
  .post(
    authController.protect,
    authController.restrictTo('admin', 'lead-guide'),
    restaurantController.createRestaurant
  );

router
  .route('/:id')
  .get(restaurantController.getRestaurant)
  .patch(
    authController.protect,
    authController.restrictTo('admin', 'lead-guide'),
    restaurantController.updateRestaurant
  )
  .delete(
    authController.protect,
    authController.restrictTo('admin', 'lead-guide'),
    restaurantController.deleteRestaurant
  );

module.exports = router;
